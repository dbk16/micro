const db = require("../models");
const users = db.users
module.exports={
	login: login
}
async function login(req,res){
	const email = req.body.email
	const password = req.body.password

	
	if (!req.body.email) {
	    res.status(400).send({ status:0, message: "Email can not be empty!" });
	    return;
	}
	if (!req.body.password) {
	    res.status(400).send({ status:0, message: "Password can not be empty!" });
	    return;
	}
	
	users.find({ email,password })
    .then(data => {
      res.send({ status:1,data});
    }).catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while retrieving tutorials."
      });
    });
}