const express = require("express");
const bodyParser = require("body-parser");
var controllers = require('./app/controllers');
const db = require('./app/models');
const app = express();

var urlencodedParser = bodyParser.urlencoded({ extended: true });
// parse requests of content-type - application/json
app.use(bodyParser.json());

// parse requests of content-type - application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: true }));

// simple route
app.get("/", (req, res) => {
  res.json({ message: "Welcome to micro application." });
});
app.post('/register',urlencodedParser,controllers.userSignup);
app.post('/signin',urlencodedParser,controllers.userLogin);

db.mongoose
  .connect(db.url, {
    useNewUrlParser: true,
    useUnifiedTopology: true
  })
  .then(() => {
    console.log("Connected to the database!");
  })
  .catch(err => {
    console.log("Cannot connect to the database!", err);
    process.exit();
  });
// set port, listen for requests
const PORT = process.env.PORT || 5000;
app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}.`);
});